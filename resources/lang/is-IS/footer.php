<?php

return [

    'version'               => 'Útgáfa',
    'powered'               => 'Keyrt af Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Opinn bókhalds hugbúnaður',

];
