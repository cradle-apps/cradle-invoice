<?php

return [

    'installed_version'     => 'Uppsett útgáfa',
    'latest_version'        => 'Síðasta útgáfa',
    'update'                => 'Uppfæra Cradle Invoice í :version version',
    'changelog'             => 'Breytingaskrá',
    'check'                 => 'Haka',
    'new_core'              => 'Ný útgáfa af Cradle Invoice er í boði.',
    'latest_core'           => 'Til hamingju! Þú hefur nýjustu útgáfuna af Cradle Invoice. Framtíðar öryggisuppfærslur verða settar upp sjálfkrafa.',
    'success'               => 'Uppfærsluferillin hefur klárst.',
    'error'                 => 'Uppfærsluferillin hefur misheppnast, reyndu aftur.',

];
