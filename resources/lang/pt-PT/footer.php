<?php

return [

    'version'               => 'Versão',
    'powered'               => 'Desenvolvido por Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Software de contabilidade gratuito',

];
