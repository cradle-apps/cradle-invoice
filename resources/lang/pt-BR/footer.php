<?php

return [

    'version'               => 'Versão',
    'powered'               => 'Desenvolvido por Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Software de contabilidade gratuito',
    'powered_by'            => 'Desenvolvido por',
    'tag_line'              => 'Envie faturas, acompanhe despesas e automatize a contabilidade com o Cradle Invoice. :get_started_url',
    'get_started'           => 'Comece agora',

];
