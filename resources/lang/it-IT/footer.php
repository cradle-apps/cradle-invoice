<?php

return [

    'version'               => 'Versione',
    'powered'               => 'Powered By Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Free Accounting Software',

];
