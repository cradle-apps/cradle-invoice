<?php

return [

    'version'               => '버전',
    'powered'               => 'Cradle Invoice 제공',
    'link'                  => 'https://usecradleapps.com',
    'software'              => '무료 회계 소프트웨어',

];
