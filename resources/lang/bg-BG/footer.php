<?php

return [

    'version'               => 'Версия',
    'powered'               => 'С подкрепата на Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Безплатен счетоводен софтуер',
    'powered_by'            => 'С подкрепата на',
    'tag_line'              => 'Изпращайте фактури, проследявайте разходите и автоматизирайте счетоводството с Cradle Invoice. :get_started_url',
    'get_started'           => 'Да започваме',

];
