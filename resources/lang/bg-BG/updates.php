<?php

return [

    'installed_version'     => 'Инсталирана версия',
    'latest_version'        => 'Последна версия',
    'update'                => 'Актуализиране на Cradle Invoice до :version версия',
    'changelog'             => 'Списък на промените',
    'check'                 => 'Проверете',
    'new_core'              => 'Актуализирана версия на Cradle Invoice е налична.',
    'latest_core'           => 'Поздравления! Вие имате последната версия на Cradle Invoice. Бъдещите актуализации ще се прилагат автоматично.',
    'success'               => 'Процес на актуализиране е завършен успешно.',
    'error'                 => 'Процес на актуализиране беше прекъснат, моля, опитайте отново.',

];
