<?php

return [

    'version'               => 'Версія',
    'powered'               => 'Зроблено в Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Безкоштовна Бухгалтерська Програма',

];
