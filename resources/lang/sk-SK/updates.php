<?php

return [

    'installed_version'     => 'Nainštalovaná verzia',
    'latest_version'        => 'Najnovšia verzia',
    'update'                => 'Aktualizácia Cradle Invoice-u na :version verziu',
    'changelog'             => 'Zoznam zmien',
    'check'                 => 'Skontrolovať',
    'new_core'              => 'K dispozícii je aktualizovaná verzia z Cradle Invoice.',
    'latest_core'           => 'Gratulujem! Máte najnovšiu verziu Cradle Invoice. Budúce aktualizácie zabezpečenia sa použije automaticky.',
    'success'               => 'Proces aktualizácie bol úspešne dokončený.',
    'error'                 => 'Proces aktualizácie zlyhal, prosím, skúste to znova.',

];
