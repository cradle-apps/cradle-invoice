<?php

return [

    'version'               => 'Verzia',
    'powered'               => 'Powered By Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Zadarmo účtovný softvér',

];
