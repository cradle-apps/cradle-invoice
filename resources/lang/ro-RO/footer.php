<?php

return [

    'version'               => 'Versiunea',
    'powered'               => 'Oferit de Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Program de contabilitate gratuit',
    'powered_by'            => 'Oferit de',
    'tag_line'              => 'Trimite facturi, urmărește cheltuielile și automatizează contabilitatea cu Cradle Invoice. :get_started_url',
    'get_started'           => 'Începe',

];
