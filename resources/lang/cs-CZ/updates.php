<?php

return [

    'installed_version'     => 'Nainstalovaná verze',
    'latest_version'        => 'Nejnovější verze',
    'update'                => 'Aktualizovat Cradle Invoice na verzi :version',
    'changelog'             => 'Seznam změn',
    'check'                 => 'Zkontrolovat',
    'new_core'              => 'K dispozici je aktualizovaná verze Cradle Invoice.',
    'latest_core'           => 'Blahopřejeme! Máte nejnovější verzi Cradle Invoice. Budoucí aktualizace budou nainstalovány automaticky.',
    'success'               => 'Proces aktualizace byl úspěšně dokončen.',
    'error'                 => 'Proces aktualizace se nezdařil, prosím, zkuste to znovu.',

];
