<?php

return [

    'version'               => 'Verze',
    'powered'               => 'Powered By Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Účetní software zdarma',

];
