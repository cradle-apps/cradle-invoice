<?php

return [

    'version'               => 'גירסה',
    'powered'               => 'מופעל על ידי Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'תוכנת הנהלת חשבונות חינם',

];
