<?php

return [

    'version'               => 'Верзија',
    'powered'               => 'Овозможенo од Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Бесплатен сметководствен софтвер',

];
