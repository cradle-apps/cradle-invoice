<?php

return [

    'version'               => 'Versija',
    'powered'               => 'Darbojas, izmantojot Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Bezmaksas grāmatvedības programma',
    'powered_by'            => 'Darbojas ar',
    'tag_line'              => 'Nosūtiet rēķinus, izsekojiet izdevumiem un automatizējiet grāmatvedību, izmantojot Cradle Invoice. :get_started_url',
    'get_started'           => 'Sākt tagad',

];
