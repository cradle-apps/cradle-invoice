<?php

return [

    'version'               => 'Versioni',
    'powered'               => 'Mundësuar nga Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Program Kontabiliteti Falas',

];
