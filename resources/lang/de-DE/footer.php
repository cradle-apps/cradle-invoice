<?php

return [

    'version'               => 'Version',
    'powered'               => 'Powered By Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Kostenlose Buchhaltungssoftware',
    'powered_by'            => 'Zur Verfügung gestellt von',
    'tag_line'              => 'Senden Sie Rechnungen, verfolgen Sie Ausgaben und automatisieren Sie die Abrechnung mit Cradle Invoice. :get_started_url',
    'get_started'           => 'Erste Schritte',

];
