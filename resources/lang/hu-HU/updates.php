<?php

return [

    'installed_version'     => 'Telepített verzió',
    'latest_version'        => 'Legújabb verzió',
    'update'                => 'Cradle Invoice frissítése a :version verzióra',
    'changelog'             => 'Változások',
    'check'                 => 'Ellenőrzés',
    'new_core'              => 'Az Cradle Invoice egy újabb változata elérhető.',
    'latest_core'           => 'Gratulálunk! Az Cradle Invoice legújabb verziójával rendelkezik. A jövőbeni biztonsági frissítések automatikusan települni fognak.',
    'success'               => 'A telepítés sikeresen befejeződött.',
    'error'                 => 'A telepítés sikertelen volt, kérjük, próbálja újra!',

];
