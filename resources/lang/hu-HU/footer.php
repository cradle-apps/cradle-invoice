<?php

return [

    'version'               => 'Verzió',
    'powered'               => 'Powered By Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Ingyenes könyvelő program',

];
