<?php

return [

    'version'               => 'Versie',
    'powered'               => 'Mogelijk gemaakt door Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Gratis boekhoudsoftware',

];
