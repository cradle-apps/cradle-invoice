<?php

return [

    'installed_version'     => 'Zainstalowana wersja',
    'latest_version'        => 'Najnowsza wersja',
    'update'                => 'Aktualizuj Cradle Invoice do wersji :version',
    'changelog'             => 'Lista zmian',
    'check'                 => 'Sprawdź',
    'new_core'              => 'Dostępna jest nowsza wersja Cradle Invoice.',
    'latest_core'           => 'Gratulacje! Masz najnowszą wersję Cradle Invoice. Przyszłe aktualizacje bezpieczeństwa zostaną zastosowane automatycznie.',
    'success'               => 'Proces aktualizacji został pomyślnie zakończony.',
    'error'                 => 'Aktualizacja nie powiodła się, spróbuj ponownie.',

];
