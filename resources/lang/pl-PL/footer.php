<?php

return [

    'version'               => 'Wersja',
    'powered'               => 'Wspierane przez Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Darmowe oprogramowanie księgowe',

];
