<?php

return [

    'installed_version'     => 'Versi Terpasang',
    'latest_version'        => 'Versi Terbaru',
    'update'                => 'Perbaharui Cradle Invoice ke versi :version',
    'changelog'             => 'Catatan Perubahan',
    'check'                 => 'Periksa',
    'new_core'              => 'Versi terbaru Cradle Invoice tersedia.',
    'latest_core'           => 'Selamat! Anda telah memiliki revisi terbaru Cradle Invoice. Pembaharuan keamanan untuk kedepan akan diterapkan secara otomatis.',
    'success'               => 'Proses pembaharuan telah selesai dengan baik.',
    'error'                 => 'Proses pembaharuan gagal, silakan, coba lagi.',

];
