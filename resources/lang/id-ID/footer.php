<?php

return [

    'version'               => 'Versi',
    'powered'               => 'Didukung oleh Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Perangkat Lunak Akutansi Gratis',
    'powered_by'            => 'Dipersembahkan oleh',
    'tag_line'              => 'Kirim faktur, lacak pengeluaran, dan otomatisasi akuntansi dengan Cradle Invoice. :get_started_url',
    'get_started'           => 'Memulai',

];
