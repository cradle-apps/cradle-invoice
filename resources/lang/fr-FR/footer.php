<?php

return [

    'version'               => 'Version',
    'powered'               => 'Propulsé par Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Logiciel de comptabilité gratuit',
    'powered_by'            => 'Propulsé par',
    'tag_line'              => 'Envoyer des factures, suivre les dépenses et automatiser la comptabilité avec Cradle Invoice. :get_started_url',
    'get_started'           => 'C\'est parti',

];
