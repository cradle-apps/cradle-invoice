<?php

return [

    'version'               => 'Versioon',
    'powered'               => 'Kasutatud tarkvara: Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Tasuta raamatupidamistarkvara',

];
