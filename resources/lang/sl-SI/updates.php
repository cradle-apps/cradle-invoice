<?php

return [

    'installed_version'     => 'Nameščena različica',
    'latest_version'        => 'Najnovejša različica',
    'update'                => 'Posodobitev programa Cradle Invoice za: verzija verzija',
    'changelog'             => 'Seznam sprememb',
    'check'                 => 'Preveri',
    'new_core'              => 'Na voljo je posodobljena verzija programa Cradle Invoice.',
    'latest_core'           => 'Čestitamo! Imate najnovejšo verzijo programa Cradle Invoice. Varnostne posodobitve bodo naložene samodejno.',
    'success'               => 'Nadgradnja je bila uspešno dokončana.',
    'error'                 => 'Nadgradnja ni bila uspešno dokončana.',

];
