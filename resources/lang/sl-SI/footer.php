<?php

return [

    'version'               => 'Verzija',
    'powered'               => 'Poganja Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Brezplačni program za računovodstvo',
    'powered_by'            => 'Stran poganja',
    'tag_line'              => 'Pošiljajte račune, spremljajte stroške in avtomatizirajte računovodstvo s programom Cradle Invoice. :get_started_url',
    'get_started'           => 'Začnite',

];
