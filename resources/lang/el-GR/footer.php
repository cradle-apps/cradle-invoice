<?php

return [

    'version'               => 'Έκδοση',
    'powered'               => 'Powered By Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Δωρεάν λογισμικό λογιστικής',

];
