<?php

return [

    'version'               => 'نسخه',
    'powered'               => 'طراحی شده توسط Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'نرم افزار حسابداری رایگان',

];
