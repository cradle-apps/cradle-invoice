<?php

return [

    'version'               => 'Versjon',
    'powered'               => 'Drevet med Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Gratis regnskapsprogram',
    'powered_by'            => 'Drives av',
    'tag_line'              => 'Send fakturaer, spor utgifter, og automatisér regnskap med Cradle Invoice. :get_started_url',
    'get_started'           => 'Kom i gang',

];
