<?php

return [

    'installed_version'     => 'Installert versjon',
    'latest_version'        => 'Nyeste versjon',
    'update'                => 'Oppdater Cradle Invoice til :version',
    'changelog'             => 'Endringslogg',
    'check'                 => 'Se etter oppdatering',
    'new_core'              => 'Det finnes en oppdatert versjon av Cradle Invoice.',
    'latest_core'           => 'Gratulerer! Du har den siste versjonen av Cradle Invoice.',
    'success'               => 'Oppdateringsprosessen er fullført.',
    'error'                 => 'Oppdateringsprosessen feilet. Forsøk igjen.',

];
