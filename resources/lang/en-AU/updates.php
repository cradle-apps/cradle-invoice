<?php

return [

    'installed_version'     => 'Installed Version',
    'latest_version'        => 'Latest Version',
    'update'                => 'Update Cradle Invoice to :version version',
    'changelog'             => 'Change log',
    'check'                 => 'Check',
    'new_core'              => 'An updated version of Cradle Invoice is available.',
    'latest_core'           => 'Congratulations! You have the latest version of Cradle Invoice. Future security updates will be applied automatically.',
    'success'               => 'Update process has been completed successfully.',
    'error'                 => 'Update process has failed, please, try again.',

];
