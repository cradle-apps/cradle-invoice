<?php

return [

    'version'               => 'Versi',
    'powered'               => 'Dikuasakan Oleh Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Perisian Perakaunan Percuma',

];
