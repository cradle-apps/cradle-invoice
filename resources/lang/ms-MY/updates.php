<?php

return [

    'installed_version'     => 'Versi Terpasang',
    'latest_version'        => 'Versi Terkini',
    'update'                => 'Kemas kini Cradle Invoice kepada versi :version',
    'changelog'             => 'Log Perubahan',
    'check'                 => 'Periksa',
    'new_core'              => 'Cradle Invoice versi yang dikemas kini ada tersedia.',
    'latest_core'           => 'Tahniah! Anda mempunyai Cradle Invoice versi yang terkini. Kemas kini keselamatan yang akan datang akan dipasang secara automatik.',
    'success'               => 'Proses kemas kini telah selesai dengan jayanya.',
    'error'                 => 'Proses kemas kini telah gagal, cuba semula.',

];
