<?php

return [

    'version'               => 'เวอร์ชัน',
    'powered'               => 'ขับเคลื่อน โดย Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'ซอฟต์แวร์บัญชีฟรี',

];
