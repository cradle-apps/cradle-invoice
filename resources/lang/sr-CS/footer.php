<?php

return [

    'version'               => 'Vеrzija',
    'powered'               => 'Omogućio vam je Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Slobodan softver za knjigovodstvo',

];
