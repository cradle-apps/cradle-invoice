<?php

return [

    'installed_version'     => 'Installeret version',
    'latest_version'        => 'Seneste version',
    'update'                => 'Opdatere Cradle Invoice til :version version',
    'changelog'             => 'Ændringslog',
    'check'                 => 'Kontrollér',
    'new_core'              => 'Der findes en opdateret version af Cradle Invoice.',
    'latest_core'           => 'Tillykke! Du har nu den nyeste version af Cradle Invoice. Fremtidige sikkerhedsopdateringer installeres automatisk.',
    'success'               => 'Opdateringen er gennemført.',
    'error'                 => 'Opdateringen er fejlet, prøv igen.',

];
