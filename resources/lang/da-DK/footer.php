<?php

return [

    'version'               => 'Version',
    'powered'               => 'Drevet af Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Gratis regnskabsprogram',
    'powered_by'            => 'Drevet af',
    'tag_line'              => 'Send fakturaer, spor udgifter, og automatiser regnskab med Cradle Invoice. :get_started_url',
    'get_started'           => 'Kom I Gang',

];
