<?php

return [

    'version'               => 'Верзија',
    'powered'               => 'Омогућио вам је Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Слободан рачуноводствени програм',

];
