<?php

return [

    'version'               => 'Versión',
    'powered'               => 'Powered By Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Software de Contabilidad Libre',

];
