<?php

return [

    'version'               => 'Версия',
    'powered'               => '© Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Бесплатное Биллинговое ПО',

];
