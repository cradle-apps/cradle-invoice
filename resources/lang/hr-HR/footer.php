<?php

return [

    'version'               => 'Verzija',
    'powered'               => 'Powered By Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Slobodan računovodstveni softver',

];
