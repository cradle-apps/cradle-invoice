<?php

return [

    'installed_version'     => 'Instalirana verzija',
    'latest_version'        => 'Posljednja verzija',
    'update'                => 'Ažuriraj Cradle Invoice na :version verziju',
    'changelog'             => 'Popis promjena',
    'check'                 => 'Provjera',
    'new_core'              => 'Dostupna je ažurirana Cradle Invoice verzija.',
    'latest_core'           => 'Čestitamo! Imate najnoviju Cradle Invoice verziju. Buduća sigurnosna ažuriranja primjenjivat će se automatski.',
    'success'               => 'Proces ažuriranja uspješno završen.',
    'error'                 => 'Proces ažuriranja nije uspio, molimo pokušajte ponovno.',

];
