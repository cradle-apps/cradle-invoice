<?php

return [

    'version'               => 'Versión',
    'powered'               => 'Desarrollado por Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Software de Contabilidad Libre',
    'powered_by'            => 'Desarrollado por',
    'tag_line'              => 'Enviar facturas, rastrear gastos y automatizar la contabilidad con Cradle Invoice. :get_started_url',
    'get_started'           => 'Empezar',

];
