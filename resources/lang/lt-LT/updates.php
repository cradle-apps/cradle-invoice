<?php

return [

    'installed_version'     => 'Versija',
    'latest_version'        => 'Naujausia versija',
    'update'                => 'Atnaujinti Cradle Invoice į :version versiją',
    'changelog'             => 'Pakeitimų sąrašas',
    'check'                 => 'Tikrinti',
    'new_core'              => 'Yra naujesnė Cradle Invoice versija.',
    'latest_core'           => 'Sveikiname! Jūs turite naujausią Cradle Invoice versiją. Tolimesni atnaujinimai bus įrašomi automatiškai.',
    'success'               => 'Atnaujinimas pavyko sėkmingai.',
    'error'                 => 'Įvyko klaida atnaujinant, prašome bandyti dar kartą.',

];
