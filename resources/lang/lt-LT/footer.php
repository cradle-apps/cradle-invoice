<?php

return [

    'version'               => 'Versija',
    'powered'               => 'Sukurta Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Laisva apskaitos programa',

];
