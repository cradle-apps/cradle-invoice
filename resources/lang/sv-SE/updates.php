<?php

return [

    'installed_version'     => 'Installerad version',
    'latest_version'        => 'Senaste versionen',
    'update'                => 'Uppdatera Cradle Invoice till version :version',
    'changelog'             => 'Ändringslog',
    'check'                 => 'Markera',
    'new_core'              => 'Det finns en uppdaterad version av Cradle Invoice.',
    'latest_core'           => 'Grattis! Du har den senaste versionen av Cradle Invoice. Framtida säkerhetsuppdateringar kommer att tillämpas automatiskt.',
    'success'               => 'Uppdateringen har fullföljts.',
    'error'                 => 'Uppdateringen har misslyckats, var vänlig, försök igen.',

];
