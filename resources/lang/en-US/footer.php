<?php

return [

    'version'               => 'Version',
    'powered'               => 'Powered By Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Free Accounting Software',

];
