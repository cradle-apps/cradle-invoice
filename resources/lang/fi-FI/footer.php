<?php

return [

    'version'               => 'Versio',
    'powered'               => 'Toteutettu ohjelmistolla Cradle Invoice',
    'link'                  => 'https://usecradleapps.com',
    'software'              => 'Ilmainen kirjanpito-ohjelma',

];
