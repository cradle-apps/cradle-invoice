<?php

namespace App\Listeners\Menu;

use App\Traits\Permissions;
use App\Events\Menu\AdminCreated as Event;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class ShowInAdmin
{
    use Permissions;

    /**
     * Handle the event.
     *
     * @param  $event
     * @return void
     */
    public function handle(Event $event)
    {
        $menu = $event->menu;

        $attr = ['icon' => ''];

        // Invoices
        $title = trim(trans_choice('general.invoices', 2));
        if ($this->canAccessMenuItem($title, 'read-sales-invoices')) {
            $menu->route('invoices.index', $title, [], 10, ['icon' => 'description']);
        }

        // Customers
        $title = trim(trans_choice('general.customers', 2));
        if ($this->canAccessMenuItem($title, 'read-sales-customers')) {
            $menu->route('customers.index', $title, [], 20, ['icon' => 'person']);
        }
    }
}
